import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'

function App() {
  const [count, setCount] = useState(0)

  const calcular = (operacion) => {
    const oa = document.getElementById('oa1');
    const ob = document.getElementById('ob2');
    const result = document.getElementById('result');
    let r = 0;
    switch (operacion) {
      case 'sumar':
        r = Number(oa.value) + Number(ob.value);
        break;
      case 'restar':
        r = Number(oa.value) - Number(ob.value);
        break;
      case 'multiplicar':
        r = Number(oa.value) * Number(ob.value);
        break;
      case 'dividir':
        r = Number(oa.value) / Number(ob.value);
        break;

      default:
        alert('operacion no especificada');
        break;
    }


    result.value = r;
  }

  return (
    <>
      <h1>Calculadora</h1>
      <div className="card">
        <input type="number"  name="oa" id="oa1"  required/>
        <input type="number"  name="ob" id="ob2" required/>
        <hr></hr>
        <button onClick={() => calcular('sumar')}> sumar + </button>
        <button onClick={() => calcular('restar')}> restar - </button>
        <button onClick={() => calcular('multiplicar')}> multplicar * </button>
        <button onClick={() => calcular('dividir')}> dividir / </button>
        <p>
          Resultado:
        </p>
        <input type="number"  className="card" name="result" id="result" readOnly/>
      </div>

    </>
  )
}

export default App
